## 说明
spring-boot-admin 与 spring cloud 集成用例, 整体演示包含如下三个工程:
- spring-boot-admin-eureka-server: 服务注册中心。
- spring-boot-admin-server-cloud: 管理服务端。
- spring-boot-admin-client-cloud: 管理客户端。