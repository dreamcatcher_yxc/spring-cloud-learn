package com.forezp.sbasc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SBACApplication {

    public static void main(String[] args) {
        SpringApplication.run(SBACApplication.class, args);
    }
}
