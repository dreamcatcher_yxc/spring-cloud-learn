package com.forezp.provider.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@RefreshScope
public class FooController implements EnvironmentAware {

    private Environment environment;

    @Value("${server.port}")
    private String port;

    @Value("${foo.bar}")
    private String fooBar;

    @RequestMapping("/hi")
    public String home(@RequestParam(value = "name", defaultValue = "forezp") String name) {
        return "hi " + name + " ,i am from port:" + port;
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;

    }

    @GetMapping("/prop")
    public String getProp(String name) {
        return environment.getProperty(name, "not found");
    }

    @GetMapping("/foo")
    public String getFooBar() {
        return fooBar;
    }
}
